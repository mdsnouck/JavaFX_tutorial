package user;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class UserController {

    public TextField voornaam;

    public TextField naam;

    public Label resultaat;

    private String nummerUitNaam(String voornaam, String naam) {
        String string = voornaam.trim().substring(0, 1) + naam;
        string = string.replaceAll("\\s", "").toLowerCase();
        if (string.length() > 8) {
            return string.substring(0, 8);
        } else {
            return string;
        }

    }
    
    public void maakNummer() {
        if (check(naam) & check(voornaam)) {
            resultaat.setText(
                    nummerUitNaam(voornaam.getText(), naam.getText())
            );
        } else {
            resultaat.setText("");
        }
    }
    
    /**
     * Controleer of een veld leeg is, en verander de stijl van het overeenkomstig
     * ankerpaneel al naargelang het resultaat
     */
    private boolean check (TextField field) {
        Parent parent = field.getParent();
        boolean empty = field.getText().trim().isEmpty();
        if (empty) {
            parent.getStyleClass().add ("alarm");
        } else {
            parent.getStyleClass().removeAll ("alarm");
        }
        return ! empty;
    }
}
