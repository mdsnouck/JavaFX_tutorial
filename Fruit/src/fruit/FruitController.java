/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fruit;

import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FruitController {

    public ListView<String> lijst;

    public ImageView imageView;

    private final String[] IMAGE_NAMES = {
        "aardbei",
        "ajuin",
        "ananas",
        "appel",
        "asperge",
        "banaan",
        "broccoli",
        "druiven",
        "framboos",
        "kiwi",
        "mais",
        "meloen",
        "peer",
        "peper",
        "perzik",
        "pickle",
        "pompelmoes",
        "tomaat",
        "watermeloen",
        "wortel"
    };

    private Image[] IMAGES = new Image[IMAGE_NAMES.length];

    public void initialize() {
        for(int index = 0; index<IMAGE_NAMES.length; index++){
            IMAGES[index] = new Image("fruit/images/" + IMAGE_NAMES[index] + ".png");
        }
        lijst.getSelectionModel().selectedItemProperty().addListener(
                ob -> kiesFruit()
        );
        lijst.getItems().addAll(IMAGE_NAMES);
        lijst.getSelectionModel().select(0);
    }

    private void kiesFruit() {
        imageView.setImage(IMAGES[lijst.getSelectionModel().getSelectedIndex()]);
    }
}
