package viewer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;

public class ViewerController {

    public ImageView imageView;

    private void showImageFromFile(File file) {
        Path path = file.toPath();
        try (InputStream stream = Files.newInputStream(path)) {
            imageView.setImage(new Image(stream));
        } catch (IOException ex) {
            // doe niets
        }
    }

    public void openFile() {
        FileChooser chooser = new FileChooser();
        FileChooser.ExtensionFilter fileExtensions = new FileChooser.ExtensionFilter("*.JPG", "*.jpg", "*.PNG", "*.png");
        chooser.getExtensionFilters().add(fileExtensions);

        File file = chooser.showOpenDialog(imageView.getScene().getWindow());
        if (file != null) {
            showImageFromFile(file);
        }
    }

    public void exitProgram() {
        Platform.exit();
    }

}
