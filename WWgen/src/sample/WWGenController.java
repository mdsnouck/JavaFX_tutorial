package sample;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import java.util.Random;

public class WWGenController {

    private static final Random RG = new Random ();
    private static final String LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public Label wachtwoord;
    public ChoiceBox<Integer> choiceBox;

    public void initialize () {
        choiceBox.getItems().addAll(8, 12, 16);
        choiceBox.setValue(12);
    }

    public void genereerWachtwoord(){
        wachtwoord.setText(randomWachtwoord(choiceBox.getValue()));
    }

    private static String randomWachtwoord (int lengte) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < lengte; i++) {
            b.append (LETTERS.charAt (RG.nextInt(LETTERS.length())));
        }
        return b.toString();
    }
}
