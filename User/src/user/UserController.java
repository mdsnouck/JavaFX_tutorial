package user;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class UserController {

    public Label voornaamLabel;
    public TextField voornaam;
    public Label voornaamError;

    public Label naamLabel;
    public TextField naam;
    public Label naamError;

    public Label resultaat;

    public void maakNummer() {
        checkVoornaam();
        checkNaam();

        if (voornaamError.isVisible() || naamError.isVisible()) {
            resultaat.setText("");
        } else {
            resultaat.setText(
                    nummerUitNaam(voornaam.getText(), naam.getText())
            );
        }
    }

    public void checkVoornaam(){
        checkLabel(voornaamLabel, voornaam, voornaamError);
    }

    public void checkNaam(){
        checkLabel(naamLabel, naam, naamError);
    }

    private void checkLabel(Label nameLabel, TextField field, Label errorLabel) {
        if(field.getText().trim().isEmpty()){
            errorLabel.setVisible(true);
            nameLabel.setTextFill(Color.RED);
        } else {
            errorLabel.setVisible(false);
            nameLabel.setTextFill(Color.BLACK);
        }

    }

    private String nummerUitNaam(String voornaam, String naam){
        voornaam = voornaam.trim();
        if (! voornaam.isEmpty()){
            naam = voornaam.substring(0,1) + naam;
        }
        naam = naam.replaceAll("\\s","").toLowerCase();
        if(naam.isEmpty()){
            return "(leeg)";
        } else if (naam.length() < 8) {
            return naam;
        } else {
            return naam.substring(0,8);
        }
    }
}
