/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countdown;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class CountdownController  {
    
    private int tijd;
    
    private static final int START_WAARDE = 20;
    
    public Label label;
    
    private Timeline timeline;

    public CountdownController() {
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(1.0), e -> puls())
        );
        timeline.setCycleCount(START_WAARDE); // stopt na één keer
    }
    
    private void puls() {
        if (tijd <= 1) {
            label.setText ("EINDE");
        } else {
            tijd --;
            label.setText (String.format ("%03d", tijd));
        }
        if (tijd == 10) {
            label.getStyleClass().add ("alarm");
        }
    }
        
    public void startTimer () {
        label.getStyleClass().remove ("alarm");
        tijd = START_WAARDE + 1;
        puls ();
        timeline.playFromStart();
    }
    
}
